<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e) {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e) {
        if ($request->wantsJson()) {
            $response = [
                'status' => false,
                'message' => $e->getMessage(),
                'data' => []
            ];

            if (config('app.debug')) {
                $response['data']['exception'] = get_class($e);
                $response['data']['trace'] = $e->getTrace();
            }
            $status = 400;
            if ($this->isHttpException($e)) {
                $status = $e->getStatusCode();
            }
            return response()->json($response, $status);
        }
        return parent::render($request, $e);
    }

}
