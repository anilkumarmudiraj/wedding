<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Event;
use App\Models\Budget;
use App\Models\Expense;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BudgetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::leftjoin('events', 'expenses.event_id', '=', 'events.id')
                    ->select('expenses.id', 'expenses.expenses_category', 'expenses.total_amount', 'expenses.remarks', 'expenses.contact_person', 'expenses.contact_details', 'events.id as event_id','events.name as event')
                    ->get();

        $total_expenses = array_sum(array_column($expenses->toArray(), 'total_amount'));

        $budgets = Budget::leftjoin('events', 'budgets.event_id', '=', 'events.id')
                    ->select('budgets.id', 'budgets.amount', 'budgets.remarks', 'events.id as event_id','events.name as event')
                    ->get();

        $total_budgets = array_sum(array_column($budgets->toArray(), 'amount'));

        $eve_budg_expe = $this->_budgetDetails($expenses, $budgets);

        return view('budgets.lists', compact(['expenses', 'budgets', 'eve_budg_expe', 'total_expenses', 'total_budgets']));
    }

    private function _budgetDetails($expenses, $budgets)
    {        
        $events = Event::get();
        $expenses_array = $expenses->toArray();
        $budgets_array = $budgets->toArray();
        $data = [];

        foreach ($events as $key => $event) {
            $bud_keys = array_keys(array_column($budgets_array, 'event_id'), $event->id);
            $exe_keys = array_keys(array_column($expenses_array, 'event_id'), $event->id);
            
            $data[$key]['event'] = $event->name;
            $data[$key]['budget'] = 0;
            if($bud_keys !== false)
            {
                foreach ($bud_keys as $bud_key) {
                    $data[$key]['budget'] += $budgets_array[$bud_key]['amount'];
                }
            }

            $data[$key]['expenses'] = 0;
            if($exe_keys !== false)
            {
                foreach ($exe_keys as $exe_key) {
                    $data[$key]['expenses'] += $expenses_array[$exe_key]['total_amount'];
                }
            }

            $data[$key]['balance'] = $data[$key]['budget'] - $data[$key]['expenses'];
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::where('client_id', Auth::user()->getAttributes()['client_id'])->get(['name', 'id']);

        return view('budgets.create', compact('events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $budget = new Budget;

        $budget->event_id = $request->event_id;
        $budget->amount   = $request->amount;
        $budget->remarks  = $request->remarks;

        $budget->save();

        return redirect()->action('BudgetsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $budget = Budget::find($id);
        $events = Event::get(['name', 'id']);

        $event_name = Budget::leftjoin('events', 'events.id', '=', 'budgets.event_id')
                        ->select('events.name')
                        ->lists('name');

        return view('budgets.edit', compact(['budget', 'events', 'event_name']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
                    'event_id'  => $request->event_id,
                    'amount'    => $request->amount,
                    'remarks'   => $request->remarks,
                    'updated_at'=> date('Y-m-d H:i:s')
                ];

        Budget::where('id', $id)->update($data);

        return redirect()->action('BudgetsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
