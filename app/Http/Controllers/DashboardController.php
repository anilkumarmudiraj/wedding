<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Event;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB, Response;
class DashboardController extends Controller
{
    public function showDashboard()
    {
    	$events = Event::get();

    	$invites = DB::table('event_invite_mapping')
    				->leftjoin('invites', 'event_invite_mapping.invite_id', '=', 'invites.id')
    				->groupBy('event_invite_mapping.event_id')
    				->select(DB::raw('count(event_invite_mapping.id) as no_of_invites, event_invite_mapping.event_id'))
    				->get();
    				
    	$invites = json_decode(json_encode($invites), true);
    	
    	foreach ($events as $key => $event) {
    		$data[$key]['event'] = $event->name;
    		$data[$key]['event_date'] = $event->event_date;

    		$data[$key]['days_to_go'] = floor((strtotime($event->event_date) - time())/(60*60*24));
            if($data[$key]['days_to_go'] < 0)
                $data[$key]['days_to_go'] = 0;
            
    		$k = array_search($event->id, array_column($invites, 'event_id'));

    		if($k !== false)
                $data[$key]['no_of_invites'] = $invites[$k]['no_of_invites'];
            else
                $data[$key]['no_of_invites'] = 0;
    	}

    	return View('dashboard', compact('data'));
    }
}
