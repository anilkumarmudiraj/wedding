<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Event;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Event::get();
        return view('events.lists', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientIds = \DB::table('clients')->lists('name', 'id');
        return view('events.create', compact('clientIds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
                    'name'   => $request->name,
                    'client_id'=> $request->client_id,
                    'status' => $request->status,
                    'event_date' => $request->event_date,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];

        Event::insert($data);

        return redirect()->action('EventsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $event = Event::find($id);
        $clientIds = \DB::table('clients')->lists('name', 'id');
        return view('events.edit', compact(['event', 'clientIds']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
                    'name'   => $request->name,
                    'client_id'=> $request->client_id,
                    'status' => $request->status,
                    'event_date' => $request->event_date,
                    'updated_at' => date('Y-m-d H:i:s')
                ];

        Event::where('id', $id)->update($data);

        return redirect()->action('EventsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('events')->where('id', $id)->delete();
        \DB::table('event_invite_mapping')->where('event_id', $id)->delete();
        return redirect()->back();
    }
}
