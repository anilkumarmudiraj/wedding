<?php

namespace App\Http\Controllers;

use Auth, DB;
use App\Models\Event;
use App\Models\Budget;
use App\Models\Expense;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::leftjoin('events', 'expenses.event_id', '=', 'events.id')
                    ->select('expenses.id', 'expenses.expenses_category', 'expenses.total_amount', 'expenses.remarks', 'expenses.contact_person', 'expenses.contact_details', 'events.id as event_id','events.name as event')
                    ->get();

        $total_expenses = array_sum(array_column($expenses->toArray(), 'total_amount'));

        $budgets = Budget::leftjoin('events', 'budgets.event_id', '=', 'events.id')
                    ->select('budgets.id', 'budgets.amount', 'budgets.remarks', 'events.id as event_id','events.name as event')
                    ->get();

        $total_budgets = array_sum(array_column($budgets->toArray(), 'amount'));

        $eve_budg_expe = $this->_budgetDetails($expenses, $budgets);

        return view('expenses.lists', compact(['expenses', 'budgets', 'eve_budg_expe', 'total_expenses', 'total_budgets']));
    }

    private function _budgetDetails($expenses, $budgets)
    {        
        $events = Event::get();
        $expenses_array = $expenses->toArray();
        $budgets_array = $budgets->toArray();
        $data = [];

        foreach ($events as $key => $event) {
            $bud_keys = array_keys(array_column($budgets_array, 'event_id'), $event->id);
            $exe_keys = array_keys(array_column($expenses_array, 'event_id'), $event->id);
            
            $data[$key]['event'] = $event->name;
            $data[$key]['budget'] = 0;
            if($bud_keys !== false)
            {
                foreach ($bud_keys as $bud_key) {
                    $data[$key]['budget'] += $budgets_array[$bud_key]['amount'];
                }
            }

            $data[$key]['expenses'] = 0;
            if($exe_keys !== false)
            {
                foreach ($exe_keys as $exe_key) {
                    $data[$key]['expenses'] += $expenses_array[$exe_key]['total_amount'];
                }
            }
        }

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::where('client_id', Auth::user()->getAttributes()['client_id'])->get(['name', 'id']);

        return view('expenses.create', compact('events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $budget = Budget::where('event_id', $request->event_id)->select(DB::raw('SUM(amount) as budget'))->get();
        $bud_amount = $budget[0]->budget;

        $expenses = Expense::where('event_id', $request->event_id)->select(DB::raw('SUM(total_amount) as expenses'))->get();
        $expe_amount = $expenses[0]->expenses + $request->total_amount;

        if($bud_amount < $expe_amount)
        {
            $message = 'Your Expenses are crossing your budget.Please add some amount into your Budgets.';
            return "<script type='text/javascript'>alert('$message');window.location.href='expenses';</script>";
        }

        $expense = new Expense;

        $expense->event_id       = $request->event_id;
        // $expense->expenses_category  = $request->expenses_category;
        $expense->total_amount   = $request->total_amount;
        $expense->remarks        = $request->remarks;
        $expense->contact_person = $request->contact_person;
        $expense->contact_details= $request->contact_details;

        $expense->save();

        return redirect()->action('ExpensesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::find($id);
        $events = Event::get(['name', 'id']);

        $event_name = Expense::leftjoin('events', 'events.id', '=', 'expenses.event_id')
                        ->select('events.name')
                        ->lists('name');

        return view('expenses.edit', compact('expense', 'events', 'event_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $budget = Budget::where('event_id', $request->event_id)->select(DB::raw('SUM(amount) as budget'))->get();
        $bud_amount = $budget[0]->budget;

        $expenses = Expense::where('event_id', $request->event_id)->select(DB::raw('SUM(total_amount) as expenses'))->get();
        $expe_amount = $expenses[0]->expenses + $request->total_amount;

        if($bud_amount < $expe_amount)
        {
            $message = 'Your Expenses are crossing your budget.Please add some amount into your Budgets.';
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
        
        $data = [
                    'event_id'   => $request->event_id,
                    // 'expenses_category' => $request->expenses_category,
                    'total_amount' => $request->total_amount,
                    'remarks' => $request->remarks,
                    'contact_person' => $request->contact_person,
                    'contact_details' => $request->contact_details,
                    'updated_at'=> date('Y-m-d H:i:s')
                ];

        Expense::where('id', $id)->update($data);

        return redirect()->action('ExpensesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows = Expense::where('id', $id)->delete();

        return $affectedRows;        
    }
}
