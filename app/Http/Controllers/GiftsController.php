<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GiftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = \DB::table('gifts')->get();
        return view('gifts.create', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getInviteName(Request $request)
    {
        $invite = \DB::table('invites')
                            ->select('name')
                            ->where('mobile_no', $request->get('mobile_no'))
                            ->first();

        if($invite)
            return $invite->name;
    }

    public function createGift(Request $request)
    {
        if($request->get('mobile_no') != '')
            return \DB::table('gifts')
                        ->insertGetId([
                                'mobile_no' =>  $request->get('mobile_no'),
                                'name' =>  $request->get('name'),
                                'gift_given' =>  $request->get('gift_given'),
                                'created_at' =>  date('Y-m-d H:i:s'),
                                'updated_at' =>  date('Y-m-d H:i:s')
                            ]);
    }

    public function deleteGift(Request $request)
    {
        \DB::table('gifts')
                    ->where('id', $request->get('id'))
                    ->delete();
    }

    public function downloadCsv()
    {
        \Excel::create('gifts', function($excel) {
            $excel->sheet('gifts', function ($sheet) {
                $sheet->fromArray(json_decode(json_encode(\DB::table('gifts')->select('name as Name', 'mobile_no as Amount', 'gift_given as Gift')->get()), true));
            });
        })->download('csv');
    }
}
