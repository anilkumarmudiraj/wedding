<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Event;
use App\Http\Requests;
use App\Models\IdeaType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IdeaTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = IdeaType::get();
        return view('idea-types.lists', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('idea-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
                    'name'   => $request->name,
                    'status' => $request->status,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];

        IdeaType::insert($data);

        return redirect()->action('IdeaTypesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $ideaType = IdeaType::find($id);

        return view('idea-types.edit', compact('ideaType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
                    'name'   => $request->name,
                    'status' => $request->status,
                    'updated_at' => date('Y-m-d H:i:s')
                ];

        IdeaType::where('id', $id)->update($data);

        return redirect()->action('IdeaTypesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
