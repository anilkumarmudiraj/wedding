<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IdeasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = \App\Models\Event::where('client_id', \Auth::user()->getAttributes()['client_id'])->lists('name', 'id');
        return view('ideas.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $ideaId = \DB::table('ideas')
                            ->select('id')
                            ->where('event_id', $request->get('event_id'))
                            ->where('idea_category_id', $request->get('idea_category_id'))
                            ->where('idea_type_id', $request->get('idea_type_id'))
                            ->first();

        if(!$ideaId)
        {
            $ideaId = \DB::table('ideas')
                            ->insertGetId([
                                    'event_id'  => $request->get('event_id'),
                                    'idea_category_id'  => $request->get('idea_category_id'),
                                    'idea_type_id'  => $request->get('idea_type_id'),
                                    'created_at'  => date('Y-m-d H:i:s'),
                                    'updated_at'  => date('Y-m-d H:i:s'),
                                ]);
        }else{
            $ideaId = $ideaId->id;
        }

        if($request->hasFile('idea_images'))
        {
            foreach ($request->file('idea_images') as $key => $value) {
                $file = $value;
                $fileName = uniqid().'.'.$file->getClientOriginalExtension();
                \Storage::put($fileName,  \File::get($file));

                $iData[$key]['image'] = $fileName;
                $iData[$key]['idea_id'] = $ideaId;
                $iData[$key]['created_at'] = date('Y-m-d H:i:s');
                $iData[$key]['updated_at'] = date('Y-m-d H:i:s');
            }

            \DB::table('idea_images')
                        ->insert($iData);    
        }

        if($request->has('name'))
        {
            foreach ($request->get('name') as $key => $value) {
                $sData[$key]['idea_id'] = $ideaId;
                $sData[$key]['name'] = $value;
                $sData[$key]['email'] = $request->get('email')[$key];
                $sData[$key]['mobile_no'] = $request->get('mobile_no')[$key];
                $sData[$key]['created_at'] = date('Y-m-d H:i:s');
                $sData[$key]['updated_at'] = date('Y-m-d H:i:s');

                $fileName = '';
                if($request->hasFile('images'))
                {
                    $file = $request->file('images')[$key];
                    $fileName = uniqid().'.'.$file->getClientOriginalExtension();
                    \Storage::put($fileName,  \File::get($file));
                }

                $sData[$key]['images'] = $fileName;
            }

            \DB::table('idea_suppliers')
                        ->insert($sData);
        }

        if($request->has('notes'))
        {
            foreach ($request->get('notes') as $key => $value) {
                $nData[$key]['idea_id'] = $ideaId;
                $nData[$key]['notes'] = $value;
                $nData[$key]['created_at'] = date('Y-m-d H:i:s');
                $nData[$key]['updated_at'] = date('Y-m-d H:i:s');
            }

            \DB::table('idea_notes')
                        ->insert($nData);
        }

        return redirect('/ideas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getIdeaData(Request $request)
    {
        $idea = \DB::table('ideas')
                        ->select('id')
                        ->where('idea_category_id', $request->get('idea_category_id'))
                        ->where('idea_type_id', $request->get('idea_type_id'))
                        ->where('event_id', $request->get('event_id'))
                        ->first();

        if(!$idea)
            return \Response::json([]);

        $imagesData = \DB::table('idea_images')
                            ->where('idea_id', $idea->id)
                            ->get();

        $suppliersData = \DB::table('idea_suppliers')
                            ->where('idea_id', $idea->id)
                            ->get();

        $notesData = \DB::table('idea_notes')
                            ->where('idea_id', $idea->id)
                            ->get();

        return \Response::json([
                                'imagesData' => $imagesData,
                                'suppliersData' => $suppliersData,
                                'notesData'     => $notesData
                               ]); 
    }

    public function deleteImage(Request $request)
    {
        \DB::table('idea_images')
                    ->where('id', $request->get('id'))
                    ->delete();
    }

    public function deleteSupplier(Request $request)
    {
        \DB::table('idea_suppliers')
                    ->where('id', $request->get('id'))
                    ->delete();
    }

    public function deleteNote(Request $request)
    {
        \DB::table('idea_notes')
                    ->where('id', $request->get('id'))
                    ->delete();
    }
}
