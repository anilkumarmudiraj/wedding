<?php

namespace App\Http\Controllers;

use Auth, DB, Config, Excel;
use App\Models\Event;
use App\Models\Invite;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

class InvitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::get(['name', 'id']);

        // $invites_data = $this->_invitesData();
        
        $courier_data = $this->_courierData();
        
        $delivery_details = $this->_deliveryDetails($events);

        return view('invites.lists', compact(['events', 'courier_data', 'delivery_details'/*, 'invites_data'*/]));
    }

    public function invitesData()
    {
        $data = DB::table('event_invite_mapping')
                    ->leftjoin('invites', 'event_invite_mapping.invite_id', '=', 'invites.id')
                    ->leftjoin('events', 'event_invite_mapping.event_id', '=', 'events.id')
                    ->leftjoin('users', 'invites.inviter', '=', 'users.id')
                    ->select('invites.id','events.name as event_name', 'invites.name as invite_name', 'invites.mobile_no', 'invites.address', 'invites.delivery_mode', 'invites.address_verification_status', 'invites.invitation_status', 'users.name as inviter_name')
                    ->orderBy('invites.id', 'desc')
                    ->get();

        $inviteData = [];
        $mobileNoArr = [];

        foreach ($data as $key => $value) {
            if($value->mobile_no != "") 
            {
                $inviteData[$value->mobile_no]['id'] = $value->id;
                $inviteData[$value->mobile_no]['invite_name'] = $value->invite_name;
                $inviteData[$value->mobile_no]['mobile_no'] = $value->mobile_no;
                
                if (in_array($value->mobile_no, $mobileNoArr))
                    $inviteData[$value->mobile_no]['event_name'] = $inviteData[$value->mobile_no]['event_name'] .", ". $value->event_name;
                else
                    $inviteData[$value->mobile_no]['event_name'] = $value->event_name;

                $mobileNoArr[] = $value->mobile_no;

                // $inviteData[$value->mobile_no]['event_name'][] = $value->event_name;
                $inviteData[$value->mobile_no]['inviter_name'] = $value->inviter_name;
                $inviteData[$value->mobile_no]['delivery_mode'] = Config::get('wedding.delivery_modes')[$value->delivery_mode];
                $inviteData[$value->mobile_no]['address'] = $value->address;
                // $inviteData[$value->mobile_no]['address_verification_status'] = $value->address_verification_status;
                // $inviteData[$value->mobile_no]['invitation_status'] = $value->invitation_status;

                $cStatus = ($value->invitation_status == 1) ? "checked" : "";

                $inviteData[$value->mobile_no]['delivered'] = '<div class="md-checkbox-list" style="display:flex">
                                                                    <div class="md-checkbox" style="margin-left:auto;margin-right:auto">
                                                                        <input type="checkbox" data-id="'.$value->id.'" id="checkbox-is'.$key.'" class="md-check updateIs" '.$cStatus.'>
                                                                        <label for="checkbox-is'.$key.'">
                                                                        <span></span>
                                                                        <span class="check"></span>
                                                                        <span class="box"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>';

                $inviteData[$value->mobile_no]['action'] = '<a href="/invites/'.$value->id.'/edit" id="edit" class="btn default btn-xs green-sinvitee"> Edit </a>
                                                            <a href="/invites/'.$value->id.' }}/delete" id="delete" class="btn default btn-xs green-sinvitee"> Delete </a>';
            }
        }

        $resource = new Collection($inviteData);

        return \Datatables::of($resource)->make(true);
    }

    private function _courierData()
    {
        $invites = DB::table('event_invite_mapping')
                    ->leftjoin('invites', 'event_invite_mapping.invite_id', '=', 'invites.id')
                    ->where('delivery_mode', '1')
                    ->select('event_invite_mapping.invite_id as id', 'invitation_status')
                    ->get();

        $invites = json_decode(json_encode($invites), true);

        $courier_data['total'] = count($invites);
        $courier_data['done'] = count(array_keys(array_column($invites, 'invitation_status'), '0'));
        $courier_data['not_done'] = $courier_data['total']-$courier_data['done'];

        return $courier_data;
    }

    private function _deliveryDetails($events)
    {
        $data = DB::table('event_invite_mapping')
                    ->leftjoin('invites','event_invite_mapping.invite_id', '=', 'invites.id')
                    ->select('event_invite_mapping.event_id',DB::raw('count(invites.id) as count'), 'invites.invitation_status')
                    ->groupBy(['event_invite_mapping.event_id', 'invites.invitation_status'])
                    ->get();

        $data = json_decode(json_encode($data), true);

        $completeData = [];
        foreach ($events as $i => $event) {
            $completeData[$i]['event'] = $event->name;
            $keys = array_keys(array_column($data, 'event_id'), $event->id);

            foreach ($keys as $key) {
                $completeData[$i][$data[$key]['invitation_status']] = $data[$key]['count'];
            }
        }

        return $completeData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::where('client_id', Auth::user()->getAttributes()['client_id'])->lists('name', 'id');

        $query = DB::table('users');

        if(Auth::user()->getAttributes()['type'] == 'admin' )
            $query->whereIn('type', ['user', 'admin'])->where('client_id', Auth::user()->getAttributes()['client_id']);
        else
            $query->where('id', Auth::user()->id);

        $inviters = $query->lists('name', 'id');
        
        return view('invites.create', compact(['events', 'inviters']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        if($request->get('update-status') == "1")
            return $this->update($request, $request->get('invite-id'));

        $invite = new Invite;

        $invite->name               = $request->name;
        $invite->mobile_no          = $request->mobile_no;
        $invite->inviter            = $request->inviter;
        $invite->address            = $request->address;
        $invite->delivery_mode      = $request->delivery_mode;
        $invite->invitation_status  = $request->invitation_status;
        $invite->address_verification_status = '0';//$request->address_verification_status;

        $invite->save();

        foreach ($request->event_ids as $key => $event_id) {
                $event_invites[$key]['invite_id'] = $invite->id;
                $event_invites[$key]['event_id']  = $event_id;
                $event_invites[$key]['created_at']  = date('Y-m-d H:i:s');
                $event_invites[$key]['updated_at']  = date('Y-m-d H:i:s');
        }

        DB::table('event_invite_mapping')->insert($event_invites);

        if($request->get('btn') == 'saa')
            return redirect()->action('InvitesController@create');
        else
            return redirect()->action('InvitesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invite              = Invite::find($id);
        $events              = Event::lists('name', 'id');
        
        $query = DB::table('users');

        if(Auth::user()->getAttributes()['type'] == 'superuser' )
            $query->whereIn('type', ['user', 'superuser']);
        else
            $query->where('id', Auth::user()->id);

        $inviters = $query->lists('name', 'id');

        $selectedEvents = DB::table('event_invite_mapping')
                                ->where('invite_id', $id)
                                ->lists('event_id');

        return view('invites.edit', compact(['invite', 'events', 'selectedEvents', 'inviters']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('event_invite_mapping')->where('invite_id', $id)->delete();
        Invite::where('id', $id)->update([
            'name'      => $request->name,
            'mobile_no' => $request->mobile_no,
            'address'   => $request->address,
            'inviter'   => $request->inviter,
            'delivery_mode' => $request->delivery_mode,
            'invitation_status' => $request->invitation_status,
            'address_verification_status' => $request->address_verification_status,
            ]);

        foreach ($request->get('event_ids') as $key => $value) {
            DB::table('event_invite_mapping')
                        ->insert([
                                    'invite_id' => $id,
                                    'event_id'  => $value,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                ]);
        }

        return redirect()->action('InvitesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('invites')->where('id', $id)->delete();
        return redirect()->back();
    }

    public function updateInvitationStatus(Request $request)
    {
        $invitation_status = ($request->get('action') == 'true') ? 1 : 0;

        Invite::where('id', $request->get('id'))->update(['invitation_status' => $invitation_status]);

        return $invitation_status;
    }

    public function updateAddressVerificationStatus(Request $request)
    {
        $address_verification_status = ($request->get('action') == 'true') ? 1 : 0;
        Invite::where('id', $request->get('id'))->update(['address_verification_status' => $address_verification_status]);

        return $address_verification_status;
    }

    public function getUploadInviteesPage()
    {
        $events  = Event::get(['name', 'id']);

        $content = 'sno,name,mobile,deliverymode,inviters';
        foreach ($events as $key => $event) {
            $content .= ','.$event->name;
        }
        $content .= ',address,address1,address2,address3,address4,pincode';

        $path = public_path().'/csv/invitees'.Auth::user()->id.'.csv';

        if(file_exists($path))
            unlink($path);

        file_put_contents($path, $content);

        return view('invites.upload');
    }

    public function uploadInvitees(Request $request)
    {
        $file = $request->file('file');

        if (!file_exists($file) || !is_readable($file)) return false;
        
        $header = NULL;
        $file_invitees = [];

        if (($handle = fopen($file, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, ',')) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $file_invitees[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        // $file_invitees = Excel::load($file)->get()->toArray();

        $response = $this->_validationCheck($file_invitees);
        
        if(!$response['status'])
            return \Response::json(['status' => $response['status'], 'message' => $response['message']]);
        
        $this->_insertInvitees($file_invitees);

        return redirect()->action('InvitesController@index');
    }

    private function _validationCheck($data)
    {
        $mobiles = [];
        $mob_error = [];
        $dup_error = [];
        $deliv_error = [];
        $inv_error = [];
        $eve_error = [];
        $events  = Event::get(['name', 'id']);

        foreach ($data as $key => $value) {
            if(strlen((int)$value['mobile']) < 10)
                array_push($mob_error, (int)$value['sno']);

            if(in_array((int)$value['mobile'], $mobiles))
                array_push($dup_error, (int)$value['sno']);
            else
                array_push($mobiles, (int)$value['mobile']);

            $delivery_modes = [];
            foreach(Config::get('wedding.delivery_modes') as $key=> $status)
            {
                $delivery_modes[] = strtolower($status);
            }

            array_push($delivery_modes, 'handdelivery');
            if(!in_array(strtolower($value['deliverymode']), $delivery_modes))
                array_push($deliv_error, (int)$value['sno']);

            $query = DB::table('users');
            if(Auth::user()->getAttributes()['type'] == 'superuser' )
                $query->whereIn('type', ['user', 'superuser']);
            else
                $query->where('id', Auth::user()->id);
            $inviters = $query->select(DB::raw('LOWER(name) as name, id'))->lists('name', 'id');

            if (!in_array(strtolower($value['inviters']), $inviters)) 
                array_push($inv_error, (int)$value['sno']);

            foreach ($events as $i => $event) {
                if(!in_array((int)$value[$event->name], [1,0]))
                    array_push($eve_error, (int)$value['sno']);
            }
        }
        $status = true;
        $message = '';

        if(count($mob_error))
        {
            $status = false;
            $message .= 'Mobile Nos are given wrong in line nos '.implode(',', $mob_error);
        }
        if(count($dup_error))
        {
            $status = false;
            $message .= 'Mobile Nos are repeated in line nos '.implode(',', $dup_error);
        }
        if(count($deliv_error))
        {
            $status = false;
            $message .= PHP_EOL.'Delivery Mode must be eigther (Hand Delivery) or (Courier) in line nos '.implode(',', $deliv_error);
        }
        if(count($inv_error))
        {
            $status = false;
            $message .= PHP_EOL.'Give appropriate Inviters names You can\'t add all inviters'.implode(',', array_unique($inv_error));
        }
        if(count($eve_error))
        {
            $status = false;
            $message .= PHP_EOL.'Fill as 1 or leave the event fields as Blank in line nos '.implode(',', array_unique($eve_error));
        }
        return [
            'status' => $status,
            'message'  => $message 
        ];
    }

    private function _insertInvitees($data)
    {
        $query = DB::table('users');
        if(Auth::user()->getAttributes()['type'] == 'superuser' )
            $query->whereIn('type', ['user', 'superuser']);
        else
            $query->where('id', Auth::user()->id);
        $inviters = $query->select(DB::raw('LOWER(name) as name, id'))->lists('name', 'id');

        foreach ($data as $key => $value) {
            $mobile_no = substr((int)$value['mobile'],-10);

            if(in_array(strtolower($value['deliverymode']),['hand delivery', 'handdelivery']))
                $delivery_mode = array_search('hand delivery', Config::get('wedding.delivery_modes'));
            else
                $delivery_mode = array_search('courier', Config::get('wedding.delivery_modes'));

            $inviter = array_search($value['inviters'], $inviters);

            $address = $value['address'].','.$value['address1'].','.$value['address2'].','.$value['address3'].','.$value['address4'].','.$value['pincode'];

            $invite = Invite::where('mobile_no', $mobile_no)->first();
            if(!$invite)
            {
                $invite = new Invite;

                $invite->name      = $value['name'];
                $invite->mobile_no = $mobile_no;
                $invite->address   = rtrim($address, ',');
                $invite->inviter   = $inviter;
                $invite->delivery_mode = $delivery_mode;
                $invite->invitation_status = 1;
                $invite->address_verification_status = 1;

                $invite->save();
            }

            $events  = Event::get(['name', 'id']);

            foreach ($events as $event) {
                if((int)$value[$event->name] == 1)
                {
                    $eve_inv = DB::table('event_invite_mapping')
                                ->where('event_id', $event->id)
                                ->where('invite_id', $invite->id)->first();
                    if(!$eve_inv)
                    {
                        DB::table('event_invite_mapping')->insert([
                            'event_id'  => $event->id,
                            'invite_id' => $invite->id,                        
                            'created_at'  => date('Y-m-d H:i:s'),
                            'updated_at'  => date('Y-m-d H:i:s')
                        ]);
                    }
                }
            }
        }
        return ;
    }

    public function getInviteData(Request $request)
    {
        $data['invite_data'] = DB::table('invites')->where('mobile_no', $request->get('mobile_no'))->first();

        if(!$data['invite_data'])
            return 0;

        $data['events'] = DB::table('event_invite_mapping')->where('invite_id', $data['invite_data']->id)->lists('event_id');
        return \Response::json($data);
    }

    public function exportInvitesForm()
    {
        $events = DB::table('events')->lists('name','name');
        $inviters = DB::table('users')->lists('name','name');
        $delivery_modes = Config::get('wedding.delivery_modes');
        array_unshift($events, "ALL");
        array_unshift($inviters, "All");
        array_unshift($delivery_modes, "All");
        
        return view('invites.export', compact(['events', 'inviters', 'delivery_modes']));
    }

    public function exportInvites(Request $request)
    {
        // dd($request->all());
        if($request->get('type') == 'event')
        {
            $ids = DB::table('events')
                        ->join('event_invite_mapping', 'events.id', '=', 'event_invite_mapping.event_id');

            if ($request->get('value') != '0') 
                $ids = $ids->where('events.name', 'like', '%'.$request->get('value').'%');

            $ids = $ids->lists('event_invite_mapping.invite_id');                

            $data = DB::table('invites')
                            ->select('invites.name', 'invites.mobile_no', 'users.name as inviter', 'invites.address', 'invites.delivery_mode', 'invites.invitation_status')
                            ->join('users', 'invites.inviter', '=', 'users.id')
                            ->whereIn('invites.id', $ids)
                            ->get();

            return $this->_exportData($data);
        }

        if($request->get('type') == 'inviter')
        {
            $ids = DB::table('users');
            
            if ($request->get('value') != '0')
                $ids = $ids->where('name', 'like', '%'.$request->get('value').'%');

            $ids = $ids->lists('id');

            $data = DB::table('invites')
                            ->select('invites.name', 'invites.mobile_no', 'users.name as inviter', 'invites.address', 'invites.delivery_mode', 'invites.invitation_status')
                            ->join('users', 'invites.inviter', '=', 'users.id')
                            ->whereIn('inviter', $ids)
                            ->get();

            return $this->_exportData($data);
        }

        if($request->get('type') == 'name')
        {
            $data = DB::table('invites')
                            ->select('invites.name', 'invites.mobile_no', 'users.name as inviter', 'invites.address', 'invites.delivery_mode', 'invites.invitation_status')
                            ->join('users', 'invites.inviter', '=', 'users.id')
                            ->where('invites.name', 'like', '%'.$request->get('value').'%')
                            ->get();

            return $this->_exportData($data);
        }

        if($request->get('type') == 'delivery_mode')
        {
            $data = DB::table('invites')
                            ->select('invites.name', 'invites.mobile_no', 'users.name as inviter', 'invites.address', 'invites.delivery_mode', 'invites.invitation_status')
                            ->join('users', 'invites.inviter', '=', 'users.id');

            if($request->get('value') == 1)
                $data = $data->where($request->get('type'), 0);
            elseif ($request->get('value') == 2) 
                $data = $data->where($request->get('type'), 1);

            $data = $data->get();

            return $this->_exportData($data);
        }
    }

    private function _exportData($tData)
    {
        $data = [];
        foreach ($tData as $key => $value) 
        {
            $data[$key]['name'] = $value->name;
            $data[$key]['mobile_no'] = $value->mobile_no;
            $data[$key]['inviter'] = $value->inviter;
            $data[$key]['address'] = $value->address;
            $data[$key]['delivery_mode'] = Config::get('wedding.delivery_modes.'.$value->delivery_mode);
            $data[$key]['invitation_status'] = Config::get('wedding.invitation_statuses.'.$value->invitation_status);
        }

        \Excel::create('invites', function ($excel) use ($data) {
            $excel->sheet('invites', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }
}
