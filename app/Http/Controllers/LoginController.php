<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth, Redirect;

class LoginController extends Controller
{
    public function login()
    {
        $setting = \DB::table('settings')
                        ->where('id', 1)
                        ->first();

        return view('login', compact('setting'));
    }

    public function loginUser(Request $request)
    {
        try {

            $type = \DB::table('users')->select('type')->where('email', $request->get('email'))->first()->type;

            if($type != 'admin') {
                $licence = \DB::table('users')
                                ->join('clients', 'users.client_id', '=', 'clients.id')
                                ->where('clients.start_date', '<=', date('Y-m-d'))
                                ->where('clients.end_date', '>=', date('Y-m-d'))
                                ->where('users.email', $request->get('email'))
                                ->count();

                if($licence == '0')
                    return Redirect::back()
                                    ->with(['status' => false, 'message' => 'Your licence expired, Please Contact Admin']);
            }

            if ( ! Auth::attempt($request->only(['email', 'password']), $request->get('remember_me')))
                return Redirect::back()
                                ->with(['status' => false, 'message' => 'Please check your credentials']); 

            return Redirect::to('/dashboard');
        }
        catch (FormValidationException $e)
        {
            return Redirect::back()->withInput()
                                   ->withErrors($e->getErrors());
        }
    }

    public function logoutUser()
    {
        Auth::logout();
        return Redirect::to('/login');
    }
}
