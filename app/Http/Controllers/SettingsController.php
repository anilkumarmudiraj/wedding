<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = \DB::table('settings')->where('id', $id)->first();
        return view('settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['name'] = $request->name;
        $data['description'] = $request->description;

        if($request->hasFile('logo'))
        {
            $file = $request->file('logo');
            $logo = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($logo,  \File::get($file));
            $data['logo'] = $logo;
        }

        if($request->hasFile('banner1'))
        {
            $file = $request->file('banner1');
            $banner1 = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($banner1,  \File::get($file));
            $data['banner1'] = $banner1;
        }

        if($request->hasFile('banner2'))
        {
            $file = $request->file('banner2');
            $banner2 = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($banner2,  \File::get($file));
            $data['banner2'] = $banner2;
        }

        if($request->hasFile('banner3'))
        {
            $file = $request->file('banner3');
            $banner3 = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($banner3,  \File::get($file));
            $data['banner3'] = $banner3;
        }

        if($request->hasFile('couple_image'))
        {
            $file = $request->file('couple_image');
            $couple_image = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($couple_image,  \File::get($file));
            $data['couple_image'] = $couple_image;
        }

        \DB::table('settings')
                    ->where('id', $id)
                    ->update($data);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
