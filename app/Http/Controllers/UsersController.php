<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = \DB::table('users')->whereIn('type', ['user', 'superuser'])->get();
        return view('users.lists', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientIds = \DB::table('clients')->lists('name', 'id');
        return view('users.create', compact('clientIds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'password'  => 'required',
            'email'     => 'required|unique:users',
            'profile_image'     => 'required',
        ]);

        $fileName = '';
        if($request->hasFile('profile_image'))
        {
            $file = $request->file('profile_image');
            $fileName = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($fileName,  \File::get($file));
        }

        \App\User::insert([
                'type' => 'user',
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'client_id' => $request->get('client_id'),
                'password' => bcrypt($request->get('password')),
                'profile_image' => $fileName,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientIds = \DB::table('clients')->lists('name', 'id');
        $user = \DB::table('users')->where('id', $id)->first();
        return view('users.edit', compact(['user', 'clientIds']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email'
        ]);

        if($request->hasFile('profile_image'))
        {
            $file = $request->file('profile_image');
            $fileName = uniqid().'.'.$file->getClientOriginalExtension();
            \Storage::put($fileName,  \File::get($file));
            $data['profile_image'] = $fileName;
        }

        $data['type'] = 'user';
        $data['name'] = $request->get('name');
        $data['email'] = $request->get('email');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');

        \App\User::where('id', $id)->update($data);

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('users')->where('id', $id)->delete();
        return redirect()->back();
    }

    public function changePasswordForm()
    {
        return view('users.change-password');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);

        if (\Hash::check($request->get('old_password'), \Auth::user()['password']))
        {
            $user = \App\User::find(\Auth::user()['id']);
            $user->password = bcrypt($request->get('password'));
            $user->save();  

            return redirect()->back()->with('msg', 'Password Changed Successfully');

        }else{
            return redirect()->back()->withErrors(['Old Password is Wrong']);
        }
    }
}
