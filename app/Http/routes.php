<?php

use Kodeine\Acl\Models\Eloquent\Role;
use App\User;

Route::get('/', function () {
    // return 0;

	// $user = User::find(5);
 //    $user->assignRole('user');
	// dd();
});

Route::get('/csv/create', function(){
	\File::put(storage_path()."/invites.csv", \Input::get('data'));
});

Route::get('/csv/download', function(){
	return response()->download(storage_path()."/invites.csv");
});

Route::group(['middleware' => 'guest'], function () {
	Route::get('/', 'LoginController@login');
	Route::get('login', 'LoginController@login');
	Route::post('login', 'LoginController@loginUser');
});

Route::group(['middleware' => ['auth']], function () {
	Route::get('/invites/export', 'InvitesController@exportInvitesForm');
	Route::post('/invites/export', 'InvitesController@exportInvites');
	
	Route::get('logout', 'LoginController@logoutUser');
	Route::get('dashboard', 'DashboardController@showDashboard');

	Route::resource('events', 'EventsController');
	Route::resource('invites', 'InvitesController');
	Route::resource('expenses', 'ExpensesController');
	Route::resource('budgets', 'BudgetsController');
	Route::resource('ideas', 'IdeasController');
	Route::resource('types/idea', 'IdeaTypesController');
	Route::resource('gifts', 'GiftsController');
	Route::resource('settings', 'SettingsController');
	Route::resource('users', 'UsersController');
	Route::resource('clients', 'ClientsController');

	Route::get('/datatables/invites', 'InvitesController@invitesData');

	Route::get('/upload', 'InvitesController@getUploadInviteesPage');
	Route::post('/upload', 'InvitesController@uploadInvitees');
	Route::post('/invite/is/change', 'InvitesController@updateInvitationStatus');
	Route::post('/invite/av/change', 'InvitesController@updateAddressVerificationStatus');
	Route::get('/invites/{id}/delete', 'InvitesController@destroy');
	Route::get('/events/{id}/delete', 'EventsController@destroy');
	Route::get('/users/{id}/delete', 'UsersController@destroy');
	Route::post('/get/invite-data', 'InvitesController@getInviteData');
	Route::get('/get/idea-data', 'IdeasController@getIdeaData');
	Route::get('/delete/image', 'IdeasController@deleteImage');
	Route::get('/delete/supplier', 'IdeasController@deleteSupplier');
	Route::get('/delete/note', 'IdeasController@deleteNote');
	Route::get('/get/invite/name', 'GiftsController@getInviteName');
	Route::get('/gift/create', 'GiftsController@createGift');
	Route::get('/gift/delete', 'GiftsController@deleteGift');
	Route::get('/gift/export', 'GiftsController@downloadCsv');

	Route::get('/password/change', 'UsersController@changePasswordForm');
	Route::post('/password/change', 'UsersController@changePassword');

	Route::get('datatables', function(){
		return Datatables::of(DB::table('users2')->select('id', 'name'))->make(true);
	});
});