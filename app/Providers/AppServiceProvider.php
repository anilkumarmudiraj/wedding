<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $setting = \DB::table('settings')
                            ->where('id', 1)
                            ->first();

        view()->share('setting', $setting);

        view()->composer('layouts.admin', function ($view) {
            $d = \DB::table('users')
                        ->select('clients.end_date')
                        ->join('clients', 'users.client_id', '=', 'clients.id')
                        ->where('users.id', \Auth::user()->id)
                        ->first();

            if($d)
                $view->end_date = $d->end_date;
            else
                $view->end_date = '';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
