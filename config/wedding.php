<?php

return [
	'statuses' => ['Active', 'Inactive'],
	'inviters'  => ['dad', 'mom'],
	'delivery_modes' => ['Hand Delivery', 'Courier'],
	'address_verification_status' => ['verified', 'unverified'],
	'invitation_statuses' => ['No', 'Yes'],
	'ideas' => [
		'Indivdual' => ['Dress', 'Make Up', 'Gifts For Couple Ideas'],
		'Event Venue Works' => ['Venue', 'Transport', 'Decor', 'Food & Beverage', 'Cake', 'Entertainment', 'Photography & Videography'],
		'Other Works' => ['Wedding Cards', 'Gifts With Wedding Cards', 'Designs Of Wedding']
	],
	'expenses_category' => ['High', 'Low'],
	'export_type' => ['name' => 'Name', 'event' => 'Event', 'inviter' => 'Inviter', 'delivery_mode' => 'Delivery Mode']
];