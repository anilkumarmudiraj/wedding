@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
{!! HTML::style('/assets/admin/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') !!}
{!! HTML::style('/assets/admin/global/plugins/fullcalendar/fullcalendar.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/jqvmap/jqvmap/jqvmap.css') !!}
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
{!! HTML::style('/assets/admin/admin/pages/css/tasks.css') !!}
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<!-- <div class="theme-panel hidden-xs hidden-sm">
				<div class="toggler">
				</div>
				<div class="toggler-close">
				</div>
				<div class="theme-options">
					<div class="theme-option theme-colors clearfix">
						<span>
						THEME COLOR </span>
						<ul>
							<li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default">
							</li>
							<li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue">
							</li>
							<li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">
							</li>
							<li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">
							</li>
							<li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">
							</li>
							<li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2">
							</li>
						</ul>
					</div>
					<div class="theme-option">
						<span>
						Layout </span>
						<select class="layout-option form-control input-sm">
							<option value="fluid" selected="selected">Fluid</option>
							<option value="boxed">Boxed</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Header </span>
						<select class="page-header-option form-control input-sm">
							<option value="fixed" selected="selected">Fixed</option>
							<option value="default">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Top Menu Dropdown</span>
						<select class="page-header-top-dropdown-style-option form-control input-sm">
							<option value="light" selected="selected">Light</option>
							<option value="dark">Dark</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Mode</span>
						<select class="sidebar-option form-control input-sm">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Menu </span>
						<select class="sidebar-menu-option form-control input-sm">
							<option value="accordion" selected="selected">Accordion</option>
							<option value="hover">Hover</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Style </span>
						<select class="sidebar-style-option form-control input-sm">
							<option value="default" selected="selected">Default</option>
							<option value="light">Light</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Position </span>
						<select class="sidebar-pos-option form-control input-sm">
							<option value="left" selected="selected">Left</option>
							<option value="right">Right</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Footer </span>
						<select class="page-footer-option form-control input-sm">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
				</div>
			</div> -->
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Event Planner <small>Welcome</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index-2.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Welcome</a>
					</li>
				</ul>
				<!-- <div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height grey-salt" data-placement="top" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp; <span class="uppercase visible-lg-inline-block"></span>&nbsp; <i class="fa fa-angle-down"></i>
					</div>
				</div> -->
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					@if(Session::has('message'))
					<div class="alert alert-success display-hide" style="display: block;">
						<button class="close" data-close="alert"></button>
						{{ Session::get('message') }}
					</div>
					@endif
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Manage Events
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<!-- <div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="/locations/create" id="sample_editable_1_new" class="btn purple">
											Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
							<table class="table table-sevented table-bordered table-hover" id="sample_1">
								<thead>
								<tr>
									<th>
										Event Details
									</th>
									<th>
										Date of Event
									</th>
									<th>
										Days to Go
									</th>
									<th>
										Number of Invites
									</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($data as $value){ ?>
									<tr class="odd gradeX">
										<td>
											{{ $value['event'] }}
										</td>
										<td>
											{{ $value['event_date'] }}
										</td>
										<td>
											{{ $value['days_to_go'] }}
										</td>
										<td>
											{{ $value['no_of_invites'] }}
										</td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
							
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END DASHBOARD STATS -->
			<div class="clearfix">
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<!-- <label class="control-label col-md-3">Inline</label> -->
						<div class="col-md-3">
							<div class="date-picker" data-date-format="mm/dd/yyyy">
							</div>
						</div>
						<div class="col-md-3">
							<img src="static/images/{{ $setting->logo }}" style="width:249px">
						</div>
						<div class="col-md-6" style="text-align: right;display: inline;">
							<img style="width: 237px;" src="static/images/{{ $setting->couple_image }}">
						</div>
						<div class="col-md-2" style="font-size: 28px; font-weight: 500;margin-top: -183px;margin-left: 54px;">
							<span>{{ $setting->description }}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
@stop


@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/jquery.vmap.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') !!}
{!! HTML::script('/assets/admin/global/plugins/flot/jquery.flot.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/flot/jquery.flot.resize.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/flot/jquery.flot.categories.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery.pulsate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-daterangepicker/moment.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-daterangepicker/daterangepicker.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
{!! HTML::script('/assets/admin/global/plugins/fullcalendar/fullcalendar.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery.sparkline.min.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/index.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/tasks.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/components-pickers.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo features
   ComponentsPickers.init();
});
</script>
<!-- END JAVASCRIPTS -->
@stop
