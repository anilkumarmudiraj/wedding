@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/star-rating/lib/jquery.raty.css') !!}
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->

@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Gift and Thank <small>form...</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Gift and Thank</a>
					</li>
				</ul>
				<!-- <div class="page-toolbar">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">Separated link</a>
							</li>
						</ul>
					</div>
				</div> -->
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Gift and Thank
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="/gifts" id="gift" method="POST" class="form-horizontal">
								<div class="form-body">
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									<div class="scan_status alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Scanned Successfully
									</div>
									<!-- <h3 class="form-section">Personal Info</h3> -->
									<div class="gift-row">
										<div class="row">
											<div class="col-md-6">
												<div class="btn-group">
													<a href="/gift/export" id="sample_editable_1_new" class="btn purple">
													Export
													</a>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<div class="col-md-3">
														<h4>Name</h4>
													</div>
													<div class="col-md-3">
														<h4>Amount</h4>
													</div>
													<div class="col-md-3">
														<h4>Gift Given</h4>
													</div>
												</div>
											</div>
										</div>
										@foreach($data as $value)
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<div class="col-md-3">
														<input type="text" readonly name="name" value="{{ $value->name }}" placeholder="Name" readonly="" data-required="1" class="form-control input-field"/>
													</div>
													<div class="col-md-3">
														<input type="text" readonly name="mobile_no" value="{{ $value->mobile_no }}" placeholder="Amount" data-required="1" class="form-control input-field"/>
													</div>
													<div class="col-md-3">
														<input type="text" readonly name="gift_given" value="{{ $value->gift_given }}" placeholder="Gifts Given" data-required="1" class="form-control input-field"/>
													</div>
													<div class="col-md-3" style="margin-top: 9px;">
														<i style="cursor:pointer;  font-size: 29px;  color: #26A69A;" data-id="{{ $value->id }}" class="fa fa-minus-circle deleteGift"></i>
													</div>
												</div>
											</div>
										</div>
										@endforeach

										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<div class="col-md-3">
														<input type="text" name="name" placeholder="Name" data-required="1" class="form-control input-field"/>
													</div>
													<div class="col-md-3">
														<input type="text" name="mobile_no" placeholder="Amount" data-required="1" class="form-control input-field"/>
													</div>
													<div class="col-md-3">
														<input type="text" name="gift_given" placeholder="Gifts Given" data-required="1" class="form-control input-field"/>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
@stop

@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
{!! HTML::script('/assets/admin/global/plugins/ckeditor/ckeditor.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/lib/markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/star-rating/lib/jquery.raty.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-validation.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-samples.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
   	FormValidation.init();
   	FormSamples.init();

   	/*$("input[name='mobile_no']").live('blur', function(){
   		$this = $(this);
   		$.ajax({
			method: "GET",
			url: "/get/invite/name",
			data: { mobile_no: $(this).val() }
		}).done(function( msg ) {
			$this.parent().parent().find("input[name='name']").val(msg)
		});
   	});*/

   	$("input[name='gift_given']").live('blur', function(){
   		$this = $(this);
   		$dom = $(this).parent().parent();

   		if($dom.find("input[name='mobile_no']").val() == '')
   			return;

   		$.ajax({
			method: "GET",
			url: "/gift/create",
			data: { mobile_no: $dom.find("input[name='mobile_no']").val(), name: $dom.find("input[name='name']").val(), gift_given: $dom.find("input[name='gift_given']").val(), thank_you_note: $dom.find("input[name='thank_you_note']").val() }
		}).done(function( msg ) {
			$(".input-field").attr('readonly', 'readonly');
			var dom = '<div class="row"><div class="col-md-12"><div class="form-group"><div class="col-md-3"><input type="text" name="mobile_no" placeholder="Amount" data-required="1" class="form-control input-field"/></div><div class="col-md-3"><input type="text" name="name" placeholder="Name" data-required="1" class="form-control input-field"/></div><div class="col-md-3"><input type="text" name="gift_given" placeholder="Gifts Given" data-required="1" class="form-control input-field"/></div></div></div></div>';
			$this.parent().parent().append('<div class="col-md-2" style="margin-top: 9px;"><i style="cursor:pointer;  font-size: 29px; color: #26A69A;" data-id="'+msg+'" class="fa fa-minus-circle deleteGift"></i></div>');
			$(".gift-row").append(dom);
		});
   	});

	$(".deleteGift").live('click', function(){
		$this = $(this);

		$.ajax({
			method: "GET",
			url: "/gift/delete",
			data: { id: $(this).attr('data-id') }
		}).done(function( msg ) { 
			$this.parent().parent().parent().parent().remove();
		});
	});
});
</script>
<!-- END JAVASCRIPTS -->
@stop