@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/star-rating/lib/jquery.raty.css') !!}
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
{!! HTML::style('/assets/admin/admin/pages/css/portfolio.css') !!}
{!! HTML::style('/assets/admin/global/plugins/fancybox/source/jquery.fancybox.css') !!}
<!-- END THEME STYLES -->

@stop


@section('maincontent')
	<style type="text/css">
		span.label {
			cursor: pointer;
		}
	</style>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Create Idea <small>form...</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Ideas</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Create Idea
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
									<!-- 	<div class="btn-group">
											<a href="/types/idea" id="sample_editable_1_new" class="btn purple">
											Idea Types <i class="fa"></i>
											</a>
										</div>
									</div> -->
									<div class="col-md-6">
										<!-- <div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div> -->
									</div>
								</div>
							</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="/ideas" id="event" method="POST" class="form-horizontal"   enctype='multipart/form-data'>
								<input type="hidden" name="idea_category_id">
								<input type="hidden" name="idea_type_id">
								<div class="form-body">
									<h3 class="form-section" style="display: table;margin-left: auto;margin-right: auto;">Ideas Entry</h3>
									<div class="row">
										<div class="col-md-6  col-md-offset-2">
											<div class="form-group">
												<label class="control-label col-md-4">Select Event <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('event_id', $events, '', ['class' => 'select2_category form-control', 'data-placeholder' => 'Choose a status', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row" style="display:none">
									<div class="col-md-8 col-md-offset-2">
										<div class="table-scrollable">
											<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th>
															Indivdual works
														</th>
														<th>
															Event Venue Works
														</th>
														<th>
															Other Works
														</th>
													</tr>
												</thead>
												<tbody class="catTable">
													<tr>
														<td data-cat-id="1" data-type-id="1">
															<span class="label label-sm label-success dress">Dress</span>
														</td>
														<td data-cat-id="2" data-type-id="1">
															<span class="label label-sm label-success">Venue</span>
														</td>
														<td data-cat-id="3" data-type-id="1">
															<span class="label label-sm label-success">Wedding Cards</span>
														</td>
													</tr>
													<tr>
														<td data-cat-id="1" data-type-id="2">
															<span class="label label-sm label-success">Make Up</span>
														</td>
														<td data-cat-id="2" data-type-id="2">
															<span class="label label-sm label-success">Transport</span>
														</td>
														<td data-cat-id="3" data-type-id="2">
															<span class="label label-sm label-success">Gifts With Wedding Cards</span>
														</td>
													</tr>
													<tr>
														<td data-cat-id="1" data-type-id="3">
															<span class="label label-sm label-success">Gifts For Couple </span>
														</td>
														<td data-cat-id="2" data-type-id="3">
															<span class="label label-sm label-success">Decor</span>
														</td>
														<td data-cat-id="3" data-type-id="3">
															<span class="label label-sm label-success">Designs Of Wedding</span>
														</td>
													</tr>
													<tr>
														<td></td>
														<td data-cat-id="2" data-type-id="4">
															<span class="label label-sm label-success">Food & Beverage</span>
														</td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td data-cat-id="2" data-type-id="5">
															<span class="label label-sm label-success">Cake</span>
														</td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td data-cat-id="2" data-type-id="6">
															<span class="label label-sm label-success">Entertainment</span>
														</td>
														<td></td>
													</tr>
													<tr>
														<td></td>
														<td data-cat-id="2" data-type-id="7">
															<span class="label label-sm label-success">Photography & Videography</span>
														</td>
														<td></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="row tabs" style="display:none">
									<div class="col-md-10 col-md-offset-1"> 
										<!-- <div class="portlet box yellow">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-gift"></i>Styled Tabs
												</div>
												<div class="tools">
													<a href="javascript:;" class="collapse">
													</a>
													<a href="#portlet-config" data-toggle="modal" class="config">
													</a>
												</div>
											</div> -->
											<div class="portlet-body">
												<h3>Entry</h3>
												<div class="tabbable-line">
													<ul class="nav nav-tabs ">
														<li class="active">
															<a href="#tab_15_1" data-toggle="tab">
															Upload Images </a>
														</li>
														<li>
															<a href="#tab_15_2" data-toggle="tab">
															Suppliers </a>
														</li>
														<li>
															<a href="#tab_15_3" data-toggle="tab">
															Notes </a>
														</li>
													</ul>
													<div class="tab-content">
														<div class="tab-pane active" id="tab_15_1">
															<div class="form-body">
																<div class="row addImages-append">
																	<div class="col-md-6  col-md-offset-2">
																		<div class="form-group">
																			<label class="control-label col-md-2">Image</label>
																			<div class="col-md-8">
																				<input type="file" name="idea_images[]" class="form-control" id="idea_images"  multiple="multiple">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-12">
																	<div class="row mix-grid addIdeaImages">

																		<a class="mix-preview fancybox-button" data-rel="fancybox-button"></a>
																	</div>
																</div>
															</div>
														</div>
														<div class="tab-pane" id="tab_15_2">
															<div class="form-body">
																<div class="row">
																	<div class="col-md-6 col-md-offset-4" style="margin-bottom: 12px">
																		<i style="cursor:pointer;  font-size: 29px;  color: #26A69A;" class="fa fa-plus-circle addSuppliers"></i>
																	</div>
																</div>
																<div class="addSuppliersHere">
																	
																</div>
															</div>
														</div>
														<div class="tab-pane" id="tab_15_3">
															<div class="form-body">
																<div class="row">
																	<div class="col-md-6 col-md-offset-4" style="margin-bottom: 12px">
																		<i style="cursor:pointer;  font-size: 29px;  color: #26A69A;" class="fa fa-plus-circle addNotes"></i>
																	</div>
																</div>
																<div class="addNotesHere">
																	
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

										<!-- </div> -->
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn purple">Submit</button>
											<a href="/events" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
@stop

@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
{!! HTML::script('/assets/admin/global/plugins/ckeditor/ckeditor.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/lib/markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/star-rating/lib/jquery.raty.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-mixitup/jquery.mixitup.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/fancybox/source/jquery.fancybox.pack.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-validation.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-samples.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/portfolio.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
   	FormValidation.init();
   	FormSamples.init();

   	$(".addSuppliers").click(function(){
   		addSuppliers();
   	});

   	$(".addNotes").click(function(){
   		addNotes();
   	});

   	$("span.label").click(function(){
   		var data_cat_id = $(this).closest("td").attr("data-cat-id");
   		var data_type_id = $(this).closest("td").attr("data-type-id")
   		$("input[name='idea_category_id']").val(data_cat_id);
   		$("input[name='idea_type_id']").val(data_type_id);

   		$('span.label-sm').removeClass('label-warning').addClass('label-success');
   		$(this).removeClass('label-success').addClass('label-warning');

   		$.ajax({
			method: "GET",
			url: "/get/idea-data",
			data: { idea_category_id: data_cat_id, idea_type_id: data_type_id, event_id: $("select[name='event_id']").val() }
		}).done(function( msg ) {
			$(".addIdeaImages").html("");
			$(".addSuppliersHere").html("");
			$(".addNotesHere").html("");

			$(".tabs").css("display", "initial");

			if(msg.length == 0)
				return;

		    $.each( msg.imagesData, function( key, value ) {
				var imageDom = '<div class="col-md-3 col-sm-4 mix category_1 mix_all"  style="display: block;  opacity: 1;"><div class="mix-inner"><img class="img-responsive" src="static/images/'+value.image+'" alt=""><div class="mix-details"><h4></h4><a class="mix-link"><i data-id="'+value.id+'" class="fa fa-link deleteImg"></i></a><a class="mix-preview fancybox-button" href="static/images/'+value.image+'" title="" data-rel="fancybox-button"><i class="fa fa-search"></i></a></div></div></div>';
				$(".addIdeaImages").append(imageDom);				
			});

			$.each(msg.suppliersData, function(key, value){				
				var supplierDom = '<div class="col-md-12 addedElement"><div class="form-group"><label class="col-md-2"><input type="text" name="name[]" value="'+value.name+'" placeholder="Name" data-required="1" class="form-control" disabled/></label><div class="col-md-3"><input type="text" name="email[]" value="'+value.email+'" disabled placeholder="Email" data-required="1" class="form-control"/></div><div class="col-md-2"><input type="text" name="mobile_no[]" value="'+value.mobile_no+'" disabled placeholder="Mobile No." data-required="1" class="form-control"/></div><div class="col-md-3"><a href="static/images/'+value.images+'" target="_blank" ><img width="100px" src="static/images/'+value.images+'"></a></div><div class="col-md-2"><i style="cursor:pointer;  margin-top: 13px;  font-size: 29px;  color: #26A69A;" class="fa fa-minus-circle" data-id="'+value.id+'" onclick="deleteSupplier(this)"></i></div></div></div>';
				$(".addSuppliersHere").append(supplierDom);
			});

			$.each(msg.notesData, function(key, value){
				var noteDom = '<div class="row addedElement"><div class="col-md-12"><div class="form-group"><label class="control-label col-md-2">Notes <span class="required">* </span></label><div class="col-md-6"><textarea disabled class="form-control" name="notes[]" rows="5">'+value.notes+'</textarea></div><div class="col-md-2"><i style="cursor:pointer;  margin-top: 13px;  font-size: 29px;  color: #26A69A;" class="fa fa-minus-circle" data-id="'+value.id+'" onclick="deleteNote(this)"></i></div></div></div></div>';
				$(".addNotesHere").append(noteDom);
			});
			
		});
   	});

   	$(".deleteImg").live('click', function(){
   		$(this).parent().parent().parent().remove();
   		$.ajax({
		  	method: "GET",
		  	url: "/delete/image",
		  	data: { id: $(this).attr('data-id') }
		});				
   	});

   	$("select[name='event_id']").change(function(){
   		$('span.label-sm').removeClass('label-warning').addClass('label-success');
   		$(".tabs").css("display", "none");
   		$(".dress").click();
   	});

   	$(".dress").click();
});

function addSuppliers()
{
	var dom = '<div class="col-md-12 addedElement"><div class="form-group"><label class="col-md-2"><input type="text" name="name[]" placeholder="Name" data-required="1" class="form-control"/></label><div class="col-md-3"><input type="text" name="email[]" placeholder="Email" data-required="1" class="form-control"/></div><div class="col-md-2"><input type="text" name="mobile_no[]" placeholder="Mobile No." data-required="1" class="form-control"/></div><div class="col-md-3"><input type="file" name="images[]" class="form-control" id="images"></div><div class="col-md-2"><i style="cursor:pointer;  margin-top: 13px;  font-size: 29px;  color: #26A69A;" class="fa fa-minus-circle" onclick="deleteSupplier(this)"></i></div></div></div>';
	$(".addSuppliersHere").append(dom);
}

function addNotes()
{
	var dom = '<div class="row addedElement"><div class="col-md-12"><div class="form-group"><label class="control-label col-md-2">Notes <span class="required">* </span></label><div class="col-md-6"><textarea class="form-control" name="notes[]" rows="5"></textarea></div><div class="col-md-2"><i style="cursor:pointer;  margin-top: 13px;  font-size: 29px;  color: #26A69A;" class="fa fa-minus-circle" onclick="deleteNote(this)"></i></div></div></div></div>';
	$(".addNotesHere").append(dom);
}

function deleteSupplier(obj)
{
	$(obj).closest(".addedElement").remove();
	$.ajax({
	  	method: "GET",
	  	url: "/delete/supplier",
	  	data: { id: $(obj).attr('data-id') }
	});
}

function deleteNote(obj)
{
	$(obj).closest(".addedElement").remove();
	$.ajax({
	  	method: "GET",
	  	url: "/delete/note",
	  	data: { id: $(obj).attr('data-id') }
	});
}

</script>
<!-- END JAVASCRIPTS -->
@stop