@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/star-rating/lib/jquery.raty.css') !!}
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->

@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Create Invite <small>form...</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Create Invite</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">Separated link</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Create Invite
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="/invites" id="invite" method="POST" class="form-horizontal">
								<div class="form-body">
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
									<div class="scan_status alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Scanned Successfully
									</div>
									<!-- <h3 class="form-section">Personal Info</h3> -->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Mobile No.<span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<input type="text" tabindex="1" name="mobile_no" data-required="1" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Name <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													<input type="text" tabindex="1" name="name" data-required="1" class="form-control"/>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Address <span></span>
												</label>
												<div class="col-md-8">
													<input type="text" tabindex="1" name="address" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Events <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('event_ids[]', $events, '', ['class' => 'select2_category form-control event-class', 'multiple' => 'multiple', 'data-placeholder' => 'Choose a Event', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Inviter <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('inviter', $inviters, '', ['class' => 'select2_category form-control inviter-class', 'data-placeholder' => 'Choose a status', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Delivered <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('invitation_status', Config::get('wedding.invitation_statuses'), 0, ['class' => 'select2_category form-control invitation-status-class', 'data-placeholder' => 'Choose a status', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Delivery Mode <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('delivery_mode', Config::get('wedding.delivery_modes'), 1, ['class' => 'select2_category form-control delivery-mode-class', 'data-placeholder' => 'Choose a Delivery Mode', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div>
										<!-- <div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-4">Address Verification Status <span class="required">
												* </span>
												</label>
												<div class="col-md-8">
													{!! Form::select('address_verification_status', Config::get('wedding.address_verification_status'), '', ['class' => 'select2_category form-control address-verification-status-class', 'data-placeholder' => 'Choose a Verification Status', 'tabindex' => '1']) !!}
												</div>
											</div>
										</div> -->																			
									</div>
								</div>
								<input type="hidden" name="update-status" value="0">
								<input type="hidden" name="invite-id">
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" value="saa" name="btn" class="btn purple" tabindex="1">Save And Add</button>
											<button type="submit" class="btn purple" tabindex="1">Submit</button>
											<a href="/invites" class="btn default" tabindex="1">Cancel</a>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
@stop

@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') !!}
{!! HTML::script('/assets/admin/global/plugins/ckeditor/ckeditor.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-markdown/lib/markdown.js') !!}
{!! HTML::script('/assets/admin/global/plugins/star-rating/lib/jquery.raty.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-validation.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/form-samples.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
   	FormValidation.init();
   	FormSamples.init();

   	$("input[name='mobile_no']").blur(function(){
   		$.ajax({
		  	method: "POST",
		  	url: "/get/invite-data",
		  	data: { mobile_no: $(this).val() }
		})
		.done(function( msg ) {
			if(msg == 0)
				return;

		    $("input[name='name']").val(msg['invite_data'].name);
		    $("input[name='address']").val(msg['invite_data'].address);
		    $(".event-class").val(msg['events']).trigger("change");
		    $(".inviter-class").val(msg['invite_data'].inviter).trigger("change");
		    $(".invitation-status-class").val(msg['invite_data'].invitation_status).trigger("change");
		    $(".delivery-mode-class").val(msg['invite_data'].delivery_mode).trigger("change");
		    $(".address-verification-status-class").val(msg['invite_data'].address_verification_status).trigger("change");
		    $("input[name='update-status']").val("1");
		    $("input[name='invite-id']").val(msg['invite_data'].id);
		});
   	});	
});
</script>
<!-- END JAVASCRIPTS -->
@stop