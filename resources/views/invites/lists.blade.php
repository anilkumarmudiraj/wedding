@extends('layouts.admin')

@section('pagelevelstyles')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! HTML::style('/assets/admin/global/plugins/select2/select2.css') !!}
{!! HTML::style('/assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
{!! HTML::style('/assets/admin/global/css/components-md.css') !!}
{!! HTML::style('/assets/admin/global/css/plugins-md.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/layout.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/themes/darkblue.css') !!}
{!! HTML::style('/assets/admin/admin/layout/css/custom.css') !!}
<!-- END THEME STYLES -->
@stop


@section('maincontent')
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">User Details</h4>
						</div>
						<div class="modal-body">
							
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Invites <small>List of Invites</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Invites</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->

			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					@if(Session::has('message'))
					<div class="alert alert-success display-hide" style="display: block;">
						<button class="close" data-close="alert"></button>
						{{ Session::get('message') }}
					</div>
					@endif
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user"></i>Manage Invites
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="/invites/create" id="sample_editable_1_new" class="btn purple">
											Add Invite <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											<a href="/invites/export" id="sample_editable_1_new" class="btn purple">
											Export
											</a>
										</div>
									</div>
									<!-- <div class="col-md-6">
										<div class="btn-group pull-right">
											<div class="col-md-6" style="text-align: right">
												<div class="btn-group">
													<a href="/upload" id="sample_editable_1_new" class="btn purple">
													Upload Invitees from Excel <i class="fa fa-plus"></i>
													</a>
												</div>
											</div>											
										</div>
									</div> -->
								</div>
							</div>
							
							<div class="row">
								<!-- <div class="col-md-6 offset-col-md-3">
									<div class="row">
										<div class="col-md-6">
											<h3 class="form-section">Courier Tracking</h3>
										</div>
									</div>
									<table class="table table-sevented table-bordered table-hover">
										<thead>
										<tr>
											<th>
												Total Courier Sent
											</th>
											<th>
												Courier Recieved
											</th>
											<th>
												Courier Pending
											</th>
										</tr>
										</thead>
										<tbody>
										<tr class="odd gradeX">
											<td>
												{{ $courier_data['total'] }}
											</td>
											<td>
												{{ $courier_data['done'] }}
											</td>
											<td>
												{{ $courier_data['not_done'] }}
											</td>
										</tr>
										</tbody>
									</table>
								</div> -->

								<div class="col-md-6 offset-col-md-3">
									<div class="row">
										<div class="col-md-6">
											<h3 class="form-section">Event Delivery Details</h3>
										</div>
									</div>
									<table class="table table-sevented table-bordered table-hover">
										<thead>
										<tr>
											<th rowspan="2">
												Event
											</th>
											<th colspan="2">
												Delivered 
											</th>
											<!-- <th colspan="2">
												Courier
											</th> -->
										</tr>
										<tr>
											<th>Yes</th>
											<th>No</th>
											<!-- <th>Done</th>
											<th>Yet To Do</th> -->
										</tr>
										</thead>
										<tbody>
										<?php foreach($delivery_details as $value){ ?>
										<tr class="odd gradeX">
											<td>
												{{ $value['event'] }}
											</td>
											<td>
												{{ isset($value[1]) ? $value[1] : 0 }}
											</td>
											<td>
												{{ isset($value[0]) ? $value[0] : 0 }}
											</td>
											<!-- <td>
												{{ isset($value[1][0]) ? $value[1][0] : 0 }}
											</td>
											<td>
												{{ isset($value[1][1]) ? $value[1][1] : 0 }}
											</td> -->
										</tr>
									<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<table class="table table-sinviteed table-bordered table-hover" id="users-table">
						        <thead>
									<tr>
										<th> Name </th>
										<th> Mobile No. </th>
										<th> Event </th>
										<th> Inviter </th>
										<th> Delivery Mode </th>
										<th> Address </th>
										<!-- <th>
											Address Verification Status
										</th> -->
										<th> Delivered </th>
										<th> Action </th>
									</tr>
								</thead>
						    </table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
		</div>
		<div class="td_div" style="display:none"></div>
	</div>
	<!-- END CONTENT -->
@stop


@section('pagelevelscripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! HTML::script('/assets/admin/global/plugins/select2/select2.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootbox/bootbox.min.js') !!}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{!! HTML::script('/assets/admin/global/scripts/metronic.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/layout.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/quick-sidebar.js') !!}
{!! HTML::script('/assets/admin/admin/layout/scripts/demo.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/table-managed.js') !!}
{!! HTML::script('/assets/admin/admin/pages/scripts/ui-alert-dialog-api.js') !!}
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   TableManaged.init();
   UIAlertDialogApi.init();

   	$(".updateAv").live("change", function(){
   		$.ajax({
			method: "POST",
			url: "/invite/av/change",
			data: { action: $(this).prop("checked"), id: $(this).attr("data-id")  }
		})
		.done(function( msg ) {
		  	location.reload();
		});
   	});

   	$(".updateIs").live("change", function(){
   		$.ajax({
			method: "POST",
			url: "/invite/is/change",
			data: { action: $(this).prop("checked"), id: $(this).attr("data-id")  }
		})
		.done(function( msg ) {
		  	location.reload();
		});
   	});

   	$('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/datatables/invites',
        columns: [
            { data: 'invite_name' },
            { data: 'mobile_no' },
            { data: 'event_name' },
            { data: 'inviter_name' },
            { data: 'delivery_mode' },
            { data: 'address' },
            { data: 'delivered' },
            { data: 'action' },
        ]
    });
});

/*var data = "Name,Mobile No.,Event,Inviter,Delivery Mode,Address\n";

function scrap () {
	$("#sample_1 tbody tr").each(function( index ) {
		// $(".td_div").html($(this).html());

		cnt = 1;
		$(this).find("td").each(function( index ) {
			if(cnt <= 6)
				data += '"' + ($(this).text().trim()) + '",';
			cnt++;
		});

		data += "\n"; 
	});
	
	$.ajax({
		method: "GET",
		url: "/csv/create",
		data: { data: data }
	}).done(function( msg ) {
	    document.location="/csv/download";
	    data = "Name,Mobile No.,Event,Inviter,Delivery Mode,Address\n";
	});
}*/
</script>
<!-- END JAVASCRIPTS -->
@stop
