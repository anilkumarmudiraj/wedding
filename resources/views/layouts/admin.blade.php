<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Event Planer | Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css"/>

{!! HTML::style('/assets/admin/global/plugins/font-awesome/css/font-awesome.css') !!}
{!! HTML::style('/assets/admin/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap/css/bootstrap.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/uniform/css/uniform.default.css') !!}
{!! HTML::style('/assets/admin/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}
{!! HTML::style('/assets/admin/global/plugins/loader/loader.css') !!}
<!-- END GLOBAL MANDATORY STYLES -->

@yield('pagelevelstyles')

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-md page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo ">
<div class="fixed" style="z-index: 100000;">
	<div class="container">
		<div class="content">
	    <div class="circle"></div>
	    <div class="circle1"></div>
	    </div>
	</div>
</div>
<!-- BEGIN HEADER -->
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="#">
			<img style="width:38px" src="/static/images/{{ $setting->logo }}" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					@if(Auth::user()->getAttributes()['profile_image'] != '')
						<img alt="" class="img-circle" src="/static/images/{{ Auth::user()->getAttributes()['profile_image'] }}"/>
					@endif
					<span class="username username-hide-on-mobile">
					{{ Auth::user()->getAttributes()['name'] }} </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="/logout">
							<i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<!-- <li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="/logout" class="dropdown-toggle">
					<i class="icon-logout"></i>
					</a>
				</li> -->
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu page-sidebar-menu-light" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
					<!-- <form class="sidebar-search " action="" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group"> -->
							<!-- <input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span> -->
							<!-- <img width="100px" style="margin-left: 63px;" src="static/images/"> -->
						<!-- </div>
					</form> -->
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
					@if(Auth::user()->getAttributes()['type'] == 'superuser' || Auth::user()->getAttributes()['type'] == 'user'))
					<li class="start">
						<a href="/dashboard">
						<i class="icon-home"></i>
						<span class="title">Home</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/invites">
						<i class="icon-user"></i>
						<span class="title">Invites</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/budgets">
						<i class="icon-users"></i>
						<span class="title">Budgets</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/expenses">
						<i class="icon-users"></i>
						<span class="title">Expenses</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/ideas">
						<i class="icon-users"></i>
						<span class="title">Got an Idea (Save It!)</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/gifts">
						<i class="icon-users"></i>
						<span class="title">Gift and Thank</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/password/change">
						<i class="icon-home"></i>
						<span class="title">Change Password</span>
						<span class="arrow "></span>
						</a>
					</li>
					@endif

					@if(Auth::user()->getAttributes()['type'] == 'admin')
					<li class="start">
						<a href="/events">
						<i class="icon-home"></i>
						<span class="title">Events</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/users">
						<i class="icon-home"></i>
						<span class="title">Users</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/clients">
						<i class="icon-home"></i>
						<span class="title">Clients</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/password/change">
						<i class="icon-home"></i>
						<span class="title">Change Password</span>
						<span class="arrow "></span>
						</a>
					</li>
					<li>
						<a href="/settings/1/edit">
						<i class="icon-users"></i>
						<span class="title">Settings</span>
						<span class="arrow "></span>
						</a>
					</li>
					@endif
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->

@yield('maincontent')

	
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		2015 &copy; Owned by Deena.
	</div>
	@if(!\Auth::user()->is('admin'))
        <span style="float: right;font-size: 16px;color:white"><b>Licence Valid Till:</b> {{ $end_date }}</span>
    @endif
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
{!! HTML::script('/assets/admin/global/plugins/jquery.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-migrate.min.js') !!}
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
{!! HTML::script('/assets/admin/global/plugins/jquery-ui/jquery-ui.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap/js/bootstrap.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery.blockui.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/jquery.cokie.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/uniform/jquery.uniform.min.js') !!}
{!! HTML::script('/assets/admin/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}
<!-- END CORE PLUGINS -->

@yield('pagelevelscripts')

</body>
<!-- END BODY -->
</html>